package Part1.Week2;
/*
    Implementation of a generic resizeable and iterable stack data structure using arrays
*/

import java.util.Iterator;

class Stack<T> implements Iterable<T> {

    T array[];
    int N;

    @Override
    public Iterator<T> iterator() {
        return new ListIterator() {
        };
    }

    private class ListIterator implements Iterator<T> {

        private int current = N;

        @Override
        public boolean hasNext() {
            return current > 0;
        }

        @Override
        public T next() {
            return (T) array[--current];
        }

    }

    Stack() {
        this.N = 0;
        array = (T[]) new Object[1];
    }

    T pop() {
        if (this.isEmpty()) {
            throw new RuntimeException("Stack empty");
        }

        if (array.length / N >= 4) {
            resize(array.length / 2);
        }

        T element = array[--N];
        array[N] = null;

        return element;
    }

    void push(T element) {
        array[N++] = element;
        if (array.length / N <= 2) {
            resize(array.length * 2);
        }
    }

    private void resize(int size) {
        T[] tmpArray = (T[]) new Object[size];
        for (int i = 0; i < N; i++) {
            tmpArray[i] = array[i];
        }
        array = tmpArray;
    }

    boolean isEmpty() {
        return N == 0;
    }

    int size() {
        return N + 1;
    }

}