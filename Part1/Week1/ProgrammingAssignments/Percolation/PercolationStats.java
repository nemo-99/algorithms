import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private int[] percolationCount;
    private double[] percolationProb;
    private int T;
    private double mean;
    private double stddev;

    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        percolationCount = new int[trials];
        percolationProb = new double[trials];
        T = trials;
        if (n <= 0 || T <= 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < trials; i++) {
            Percolation percolation = new Percolation(n);
            while (!percolation.percolates()) {
                int randomNumber = StdRandom.uniform(0, n * n);
                int randomRow = randomNumber / n + 1;
                int randomCol = randomNumber % n + 1;
                while (percolation.isOpen(randomRow, randomCol)) {
                    randomNumber = StdRandom.uniform(0, n * n);
                    randomRow = randomNumber / n + 1;
                    randomCol = randomNumber % n + 1;
                }
                percolation.open(randomRow, randomCol);
            }
            percolationCount[i] = percolation.numberOfOpenSites();
            percolationProb[i] = percolationCount[i] / ((double) n * n);

        }
    }

    // sample mean of percolation threshold
    public double mean() {
        mean = StdStats.mean(percolationProb);
        return mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        stddev = StdStats.stddev(percolationProb);
        return stddev;
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        double part1 = 1.96 * stddev / Math.pow(T, 0.5);
        return mean - part1;
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        double part1 = 1.96 * stddev / Math.pow(T, 0.5);
        return mean - part1;
    }

    // test client (see below)
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats percolationStats = new PercolationStats(n, T);
        System.out.println("Mean = " + percolationStats.mean());
        System.out.println("stddev = " + percolationStats.stddev());
        System.out.println("95% confidence interval = [" + percolationStats.confidenceLo() + ", "
                + percolationStats.confidenceHi() + "]");
    }

}
