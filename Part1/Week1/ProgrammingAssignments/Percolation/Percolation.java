import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private int[][] array;
    private int count;
    private WeightedQuickUnionUF Uf;
    private int n;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }

        array = new int[n][];

        for (int i = 0; i < n; i++) {
            array[i] = new int[n];
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = 0;
            }
        }

        Uf = new WeightedQuickUnionUF(n * n + 2);
        count = 0;
        this.n = n;
    }

    private int matrixToArray(int i, int j) {
        return n * i + j + 1;
    }

    private boolean ifValid(int i, int j) {
        if (i < n && i >= 0 && j >= 0 && j < n) {
            return true;
        }

        return false;
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        if (isOpen(row, col)) {
            return;
        }

        row -= 1;
        col -= 1;

        if (!ifValid(row, col)) {
            throw new IllegalArgumentException();
        }

        int tmpArray[] = { -1, +1 };
        for (int i = 0; i < tmpArray.length; i++) {
            if (ifValid(row + tmpArray[i], col) && isOpen(row + tmpArray[i] + 1, col + 1)) {
                Uf.union(matrixToArray(row + tmpArray[i], col), matrixToArray(row, col));
            }

            if (ifValid(row, col + tmpArray[i]) && isOpen(row + 1, col + tmpArray[i] + 1)) {
                Uf.union(matrixToArray(row, col), matrixToArray(row, col + tmpArray[i]));
            }
        }

        array[row][col]++;
        count++;

        if (row == 0) {
            Uf.union(0, matrixToArray(row, col));
        }

        if (row == n - 1) {
            Uf.union(matrixToArray(row, col), n * n + 1);
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        row -= 1;
        col -= 1;

        if (!ifValid(row, col)) {
            throw new IllegalArgumentException();
        }

        return array[row][col] == 1;
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        row--;
        col--;

        if (!ifValid(row, col)) {
            throw new IllegalArgumentException();
        }

        return Uf.find(0) == Uf.find(matrixToArray(row, col));
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return count;
    }

    // does the system percolate?
    public boolean percolates() {
        return Uf.find(0) == Uf.find(n * n + 1);
    }

    // test client (optional)
    public static void main(String[] args) {
        // Code for debugging
        // Percolation percolation = new Percolation(5);
        // percolation.open(1, 5);
        // percolation.open(5, 5);

        // System.out.println(percolation.numberOfOpenSites());
        // System.out.println(percolation.isFull(5, 5));
        // System.out.println(percolation.matrixToArray(5, 5));
        // System.out.println(percolation.Uf.find(26) == percolation.Uf.find(25));
    }
}
