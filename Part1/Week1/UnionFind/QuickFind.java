package algorithm1;

import edu.princeton.cs.algs4.StdIn;

public class QuickFind implements UnionFind {

	private int[] array;

	QuickFind(int N) {
		array = new int[N];
		for (int i = 0; i < N; i++) {
			array[i] = i;
		}
	}

	@Override
	public void union(int p, int q) {

		System.out.println("Union " + p + " , " + q);
		int pIndex = array[p];

		for (int i = 0; i < array.length; i++) {
			if (array[i] == pIndex) {
				array[i] = array[q];
			}
		}

	}

	@Override
	public boolean connected(int p, int q) {
		return array[p] == array[q];
	}

}
