package algorithm1;

public class QuickUnion implements UnionFind {

	int[] array;

	QuickUnion(int N) {
		array = new int[N];
		for (int i = 0; i < N; i++) {
			array[i] = i;
		}
	}

	protected int root(int i) {
		while (i != array[i]) {
			i = array[i];
		}
		return i;
	}

	@Override
	public void union(int p, int q) {
		System.out.println("Union " + p + "," + q);
		array[root(p)] = root(q);
	}

	@Override
	public boolean connected(int p, int q) {
		return root(p) == root(q);
	}

}
