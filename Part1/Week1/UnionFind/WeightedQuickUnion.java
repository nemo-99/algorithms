package algorithm1;

public class WeightedQuickUnion extends QuickUnion {

	int[] size;

	WeightedQuickUnion(int N) {
		super(N);
		size = new int[N];
		for (int i = 0; i < N; i++) {
			size[i] = 1;
		}
	}

	@Override
	public void union(int p, int q) {
		System.out.println("Union " + p + " " + q);

		int rootP = root(p);
		int rootQ = root(q);

		if (size[rootP] > size[rootQ]) {
			array[rootQ] = rootP;
			size[rootP] += size[rootQ];
		} else {
			array[rootP] = rootQ;
			size[rootQ] += size[rootP];
		}
	}

}
