package algorithm1;

public class WeightedQuickUnionWithPathShortening extends WeightedQuickUnion {

	public WeightedQuickUnionWithPathShortening(int N) {
		super(N);
	}

	@Override
	protected int root(int i) {
		while(i != array[i])
		{
			array[i] = array[array[i]];
			i = array[i];
		}
		return i;
	}

}
